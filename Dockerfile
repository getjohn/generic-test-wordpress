# syntax = docker/dockerfile:latest
#
# Get John Wordpress test harness
#
# Environment variables - see .env.sample
#
# Part based on https://github.com/markhilton/docker-php-fpm/blob/master/7.3/Dockerfile
#
FROM php:8.3-fpm

# we don't add DEBIAN_FRONTEND with ENV because it would persist into the container
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq 

# apt packages:
RUN apt-get install -y -qq wget git ssh mariadb-client unzip vim rsync nginx jq nullmailer sudo

# extensions already in: ctype curl hash iconv pic ftp mbstring mysqlnd argon2 sodium pdo_sqlite sqlite3 libedit openssl zlib pear json xml filter
# official Wordpress required exts: curl dom exif fileinfo hash json mbstring mysqli sodium openssl imagick xml zip filter gd iconv mcrypt simplexml xmlreader zlib

# for php extensions - not needed, all preinstalled except for gd:
RUN apt-get install -y -qq libjpeg62-turbo-dev libpng-dev libxml2-dev libxslt-dev libsodium-dev libzip-dev libfreetype6-dev
RUN pecl install xdebug
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install exif mysqli zip gd
# skipped install of: mcrypt xmlreader imagick
RUN docker-php-ext-enable xdebug exif mysqli zip gd
# skipped enable of bcmath intl pdo_mysql soap xsl sockets

# Composer:
RUN EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"; \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; \
    ACTUAL_SIGNATURE="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")"; \
    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]; \
    then \
        >&2 echo 'ERROR: Invalid installer signature'; \
        rm composer-setup.php; \
        exit 1; \
    fi; \
    php composer-setup.php --quiet && mv composer.phar /usr/local/bin/composer2 && chmod +x /usr/local/bin/composer2 && \
    php composer-setup.php --quiet --1 && mv composer.phar /usr/local/bin/composer1 && chmod +x /usr/local/bin/composer1 && \
    ln -s /usr/local/bin/composer2 /usr/local/bin/composer

# wp-cli
RUN curl -sS -o /usr/local/bin/wp https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
    chmod +x /usr/local/bin/wp

# Install ngrok (latest official stable from https://ngrok.com/download).
# (taken from https://github.com/ghost-inspector/docker-test-runner )
ADD https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip /ngrok.zip
RUN set -x \
 && unzip -o /ngrok.zip -d /usr/local/bin \
 && rm -f /ngrok.zip

# install wait-for
RUN apt-get install -y -qq netcat-traditional
RUN curl -sS -o wait-for-it.sh 'https://raw.githubusercontent.com/vishnubob/wait-for-it/81b1373f17855a4dc21156cfe1694c31d7d1792e/wait-for-it.sh' && \
    chmod +x wait-for-it.sh && \
    mv wait-for-it.sh /usr/local/bin/

# TOOD do we even need this?
RUN apt-get clean -qq

# forward to mailhog
COPY .docker/build/nullmailer /etc/nullmailer

# install nginx config file
COPY .docker/build/nginx/www.conf /etc/nginx/sites-enabled/default

# install php FPM config
ADD .docker/build/fpm/php.ini /usr/local/etc/php/
COPY .docker/build/fpm/pool.d/www.conf /usr/local/etc/php-fpm.d/www.conf

# install the php sendmail setting for mailhog

WORKDIR /opt/getjohn/container

COPY .docker/run .docker/run
RUN mkdir -p /tmp/sql
RUN mkdir -p /var/log/php-fpm
# this will hold the actual WP installation - we keep our .docker stuff one level above, because the user might want to map a volume onto /wordpress
RUN mkdir -p /opt/getjohn/container/wordpress

RUN wp --allow-root core download --locale=en_GB --path=/opt/getjohn/container/wordpress/
RUN wp --allow-root core config --dbname=dbname --dbuser=dbuser --dbpass=dbpass --dbhost=dbhost --locale=en_GB --skip-check --path="/opt/getjohn/container/wordpress/"

# frontend, admin, and Mailhog:
EXPOSE 80
# ngrok API:
EXPOSE 4040
# Mailhog:
EXPOSE 1025
EXPOSE 8025

# entrypoint will be overridden by bitbucket pipeline so run it manually from the pipeline script:
ENTRYPOINT [".docker/run/entrypoints/entrypoint.sh"]

# default command - our entrypoint script checks this
# If you want to examine the container, set the command to /bin/bash or "shell"
CMD ["daemon"]

