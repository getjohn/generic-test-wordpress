#!/bin/bash

set -e

[ -r Dockerfile ] || (echo $0 must be executed from the root of the Wordpress installation!; exit 1)
: ${GIT_REPO_SLUG:=getjohn/`git remote -v | grep origin | head -1 | sed -e "s@.*/@@" -e "s@\.git.*@@"`}

: ${TAGS:=`git tag --points-at HEAD`}
: ${TAGS:?No git tag detected, please tag your latest commit}
CLI_TAGS=
for i in $TAGS;do
	CLI_TAGS="$CLI_TAGS -t $GIT_REPO_SLUG:$i"
done

echo =================
echo Building $GIT_REPO_SLUG with tags $CLI_TAGS
echo =================

DOCKER_BUILDKIT=1 docker build $CLI_TAGS .


