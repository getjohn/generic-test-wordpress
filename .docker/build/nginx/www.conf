
# COULD BE OVERRIDDEN BY .docker/run/config/nginx-with-url-path.conf IF THE URL HAS A PATH

map $http_x_forwarded_proto $gj_https_flag {
    https 1;
    default 0;
}

server {
    listen 80;

    server_name _;
    set $ROOT /opt/getjohn/container/wordpress;
    set $FPM_HOST 127.0.0.1:9000;

    root $ROOT;

    location /.well-known {
    }

    location / {
        index index.html index.htm index.php;
        try_files $uri $uri/ @handler;
        expires 30d;
    }

    # deny running scripts inside other directories that they should not be in
    location ~* /(wp-content|wp-includes|wp-admin/includes)/.*\.(php|pl|py|jsp|asp|sh|cgi)$ {
        deny all;
    }

    location ~* /wp-admin/install\.php {
        deny all;
    }

    location @handler {
        rewrite /wp-admin$ $scheme://$host$uri/ permanent;
        rewrite ^/(.*) /index.php?q=$1;
    }

    location ~ \.php/ {
        rewrite ^(.*\.php)/ $1 last;
    }

    location ~ \.php$ {
        if (!-e $request_filename) { rewrite / /index.php last; }

        expires        off;
        fastcgi_pass   $FPM_HOST;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include        fastcgi_params;
        fastcgi_param  HTTPS $gj_https_flag;
    }

    # Banned locations (only reached if the earlier PHP entry point regexes don't match)
    location ~* (\.php$|\.htaccess$|\.git) {
        deny all;
    }

    access_log /var/log/nginx/access.log combined;
    error_log /var/log/nginx/error.log warn;
}
