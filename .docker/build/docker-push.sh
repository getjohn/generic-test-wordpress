#!/bin/bash

set -e

if [ -r .env ]; then
	. .env
fi

[ -r Dockerfile ] || (echo $0 must be executed from the root of the Wordpress installation!; exit 1)
: ${GIT_REPO_SLUG:=`git remote -v | grep origin | head -1 | sed -e "s@.*/@@" -e "s@\.git.*@@"`}

: ${TAGS:=`git tag --points-at HEAD`}
: ${TAGS:?No git tag detected, please tag your latest commit}

echo =================
echo Pushing getjohn/$GIT_REPO_SLUG tags $TAGS
echo =================

if [ -n "$DOCKER_HUB_USER" ]; then
	docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD
	for i in $TAGS; do
		docker push getjohn/$GIT_REPO_SLUG:$i
	done
fi

