#!/bin/bash

# Script for use when this container is used in a CI/CD pipeline for testing a composer module.
# The composer module will be in $GIT_CLONE_DIR, so we use 'studio' to explicitly require it from there.


set -e

[ -r wp-config.php ] || (echo $0 must be executed from the root of the wordpress installation!; exit 1)

: ${GIT_CLONE_DIR:?No GIT_CLONE_DIR found - set this to the path where your composer module lives}
if [ ! -r "$GIT_CLONE_DIR/composer.json" ]; then
	echo "Could not find composer.json in $GIT_CLONE_DIR - is this a composer module?"
	exit 1
fi

MODULE=`jq -r .name <$GIT_CLONE_DIR/composer.json`
: "${MODULE:?No module name found in $GIT_CLONE_DIR/composer.json}"
VERSION=`jq -r '.version // ""' <$GIT_CLONE_DIR/composer.json`
if [ -n "$VERSION" ]; then
	VERSION="^$VERSION"
fi
: "${VERSION:=@dev}"

echo -e "============================================================\nFound local module for testing: $MODULE\n=========================================================\n"
composer config repositories.local-test '{"type":"path","url":"'$GIT_CLONE_DIR'"}'
composer require $MODULE:$VERSION

