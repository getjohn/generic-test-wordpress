#!/bin/bash

set -e 

cd /opt/getjohn/container/wordpress

if [ -t 0 -a -t 1 ]; then
	/bin/bash
	exit 0
fi

echo "shell requested in non-interactive mode... I'm just gonna wait right here for a while..."
read

