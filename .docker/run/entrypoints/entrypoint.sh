#!/bin/bash
#
# main entrypoint, processes the command line arguments,
# defaults to the CMD setting in Dockerfile which is "daemon"

set -e

cd /opt/getjohn/container/wordpress
: ${WORDPRESS_DATABASE_HOST:=127.0.0.1}
: ${GJ_SERVICES_WAIT:=240}
: ${GJ_DB_STRIP:=@stripped}
: ${GJ_USER_ID:=0}
: ${GJ_WP_VERSION:=latest}

set -e

SUDO=
if [ "$GJ_USER_ID" -ne 0 ]; then
	userdel www-data || true
	groupdel www-data || true
	groupadd -g $GJ_USER_ID www-data
	useradd -u $GJ_USER_ID -g $GJ_USER_ID -d /opt/getjohn/container/wordpress -s /bin/bash www-data
	SUDO="sudo -u www-data "
fi

if [ -n "$PRE_INIT_SCRIPT" ]; then
	echo "Running pre-setup-script $PRE_INIT_SCRIPT"
	$GIT_CLONE_DIR/$PRE_INIT_SCRIPT
fi

# load compose module if provided:
if [ -n "$GIT_CLONE_DIR" ] && [ -r "$GIT_CLONE_DIR/composer.json" ]; then
	/opt/getjohn/container/.docker/run/scripts/module-load.sh
fi

if [ -r composer.json ] && [ -z "$GJ_SKIP_COMPOSER" ]; then
	composer validate --no-check-publish && composer install
fi

# if no explicit URL set, and we have a ngrok token, create a tunnel and set the URL from that:
if [ -z "$GJ_WEBSITE_URL" ] && [ -n "$NGROK_TOKEN" ]; then
	. /opt/getjohn/container/.docker/run/scripts/start-ngrok-tunnel.sh
	GJ_WEBSITE_URL=$NGROK_URL
fi


# add trailing slash if not given:
GJ_WEBSITE_URL=`echo $GJ_WEBSITE_URL|sed -E 's#/?$#/#'`
# get the path part if there is one:
URL_PATH=`echo $GJ_WEBSITE_URL | sed -E 's#.*//[^/]+(.*)/$#\1#'`

: ${GJ_WEBSITE_URL:?Please set GJ_WEBSITE_URL for local access, or provide NGROK_TOKEN for public access}

# apply the path if there is one:
if [ -n "$URL_PATH" ]; then
       echo "Updating nginx config with URL Path $URL_PATH..."
       cat ../.docker/run/config/nginx-with-url-path.conf |sed "s#{URL_PATH}#$URL_PATH#g" >/etc/nginx/sites-enabled/default
       ln -fvs . .$URL_PATH
fi

echo "Waiting for services to start..."
/opt/getjohn/container/.docker/run/scripts/wait-for-it.sh $WORDPRESS_DATABASE_HOST:3306 -t $GJ_SERVICES_WAIT

# reset:
if [ ${GJ_DB_RESET:=0} -eq 1 ]; then
	mysql -h mysql -u dbuser -pdbpass dbname -B --skip-column-names -e 'drop database dbname; create database dbname;'
	wp --allow-root core download --force --locale=en_GB --path=/opt/getjohn/container/wordpress/
	wp --allow-root core config --force --dbname="$WORDPRESS_DATABASE_NAME" --dbuser="$WORDPRESS_DATABASE_USER" --dbpass="$WORDPRESS_DATABASE_PASSWORD" --dbhost="$WORDPRESS_DATABASE_HOST" --locale=en_GB --path="/opt/getjohn/container/wordpress/"
	wp --allow-root core install --url="$GJ_WEBSITE_URL" --title="New Docker test Wordpress Site" --admin_user="gjadmin" --admin_password="gjadmin321" --admin_email="help@getjohn.co.uk"  --path="/opt/getjohn/container/wordpress/"
else
	# set the URL and DB on an existing database
	wp --allow-root config set WP_HOME "$GJ_WEBSITE_URL"
	wp --allow-root config set WP_SITEURL "$GJ_WEBSITE_URL"
	wp --allow-root config set DB_NAME "$WORDPRESS_DATABASE_NAME"
	wp --allow-root config set DB_USER "$WORDPRESS_DATABASE_USER"
	wp --allow-root config set DB_PASSWORD "$WORDPRESS_DATABASE_PASSWORD"
	wp --allow-root config set DB_HOST "$WORDPRESS_DATABASE_HOST"
fi

# import:
if [ -n "$GJ_DB_IMPORT" ] && [ ${GJ_DB_RESET:=0} -ne 1 ]; then
	mysql -h "$WORDPRESS_DATABASE_HOST" -u "$WORDPRESS_DATABASE_USER" -p"$WORDPRESS_DATABASE_PASSWORD" "$WORDPRESS_DATABASE_NAME" -B --skip-column-names -e 'drop database dbname; create database dbname;'
	wp --allow-root db import /tmp/sql/$GJ_DB_IMPORT
	# run a plugin update??
fi

# create/update our admin user:
wp --allow-root user create gjadmin help@getjohn.co.uk --role=administrator --user_pass=gjadmin321 || wp --allow-root user update gjadmin --user_pass=gjadmin321

if [ -n "$GJ_WEBSITE_OLD_URL" ]; then
	wp --allow-root search-replace "$GJ_WEBSITE_OLD_URL" "$GJ_WEBSITE_URL" --report --report-changed-only
fi

if [ -n "$POST_INIT_SCRIPT" ]; then
	echo "Running post-setup-script $POST_INIT_SCRIPT"
	$SUDO $GIT_CLONE_DIR/$POST_INIT_SCRIPT
fi

# apply the path if there is one:
if [ -n "$URL_PATH" ]; then
        echo "Updating nginx config with URL Path $URL_PATH..."
        cat /opt/getjohn/container/.docker/run/config/nginx-with-url-path.conf |sed "s#{URL_PATH}#$URL_PATH#g" >/etc/nginx/sites-enabled/default
fi

if [ "$GJ_USER_ID" -ne 0 ]; then
	cp /opt/getjohn/container/.docker/run/config/php-fpm-pool-without-user.conf /usr/local/etc/php-fpm.d/www.conf
	echo "user=www-data" >>/usr/local/etc/php-fpm.d/www.conf
	echo "group=www-data" >>/usr/local/etc/php-fpm.d/www.conf

	# also ensure wp-content is writeable:
	chown -R www-data:www-data ./wp-content/
fi

# start nginx (by default, in the background)
nginx
# php-fpm running as root, in the background
php-fpm -D -R
# start MTA to send to mailhog
service nullmailer start

chmod -R 777 /var/log

cat << EOF
========================
	Wordpress is running on $GJ_WEBSITE_URL
	Admin: ${GJ_WEBSITE_URL}wp-admin/
	Admin user: gjadmin
	Admin pass: gjadmin321
============================
EOF

if [ "$GJ_USER_ID" -ne 0 ]; then
	cat << EOF
Running as your local user ID, username "www-data"
EOF

fi

case "$@" in
ghost) /opt/getjohn/container/.docker/run/scripts/ghostinspector-test-run.sh;;
shell) $SUDO /opt/getjohn/container/.docker/run/entrypoints/magento-shell.sh;;
*) sleep infinity;;
esac
RESULT=$?

if [ -n "$GJ_DB_EXPORT" ]; then
	echo -e "=======================\nShutting down - first exporting DB:"
	cd /opt/getjohn/container/wordpress
	$SUDO wp --allow-root db export /tmp/sql/$GJ_DB_EXPORT  # use $STRIP for --exclude_tables ?
	# the sql file will be owned by root, so if it's in an external volume, ensure it's usable:
	#chmod 777 /tmp/sql/$GJ_DB_EXPORT
fi
exit $RESULT

